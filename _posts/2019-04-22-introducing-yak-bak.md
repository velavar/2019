---
title: Introducing Yak-Bak
date: 2019-04-22 00:00:00 -0400
---

You may have noticed that PyGotham's [call for proposals](https://cfp.pygotham.org)
site looks different this year. That's [Yak-Bak](https://gitlab.com/bigapplepy/yak-bak/),
the new system we're building to run our call for proposals in its entirety,
including accepting proposals, anonymization, public voting and review, and talk
selection. We'd love your feedback, and if you organize a conference that shares
PyGotham's CFP goals, we hope this tool will make your proposals process easier all
around.

## History

Since starting in 2011, PyGotham has used four different CFP setups: two that were
part of two different conference website packages,
[PaperCall](https://www.papercall.io/), and now Yak-Bak. We switched to a [static
website generator](https://gitlab.com/pygotham/2019/) in 2017 to simplify the site's
hosting needs and free up time to focus on other parts of running the conference. In
2017 and 2018, we used PaperCall for our CFP. We had a great experience with it and
will happily recommend it to smaller conferences. PaperCall significantly eased the
process of collecting proposals, but we supplemented it with additional tools both
years to help implement our voting, anonymization, and selection process.

## PyGotham's CFP process

The details of this process continue to evolve, but they always center
around the following stages:

- **Accepting proposals** is a minimum requirement. We aim to balance ease of
  proposing a talk against encouraging high quality proposals and allowing
  for effective and efficient talk review. We require a title and public
  description (for the conference program), but also request an outline and
  note about what the audience will learn from a talk. These fields help
  crystallize the intent and content of the talk to the speaker, and we find
  it makes proposals stronger and easier for the program committee to
  understand.
- **Public voting** helps inform our program committee of relative popularity
  for the purposes of talk selection and schedule balancing. This is one of
  the core features that led to the development of Yak-Bak.
- **Anonymous review** is crucial to removing bias from the talk selection
  process. A first class anonymous voting and review feature should hide
  names by default and support redaction of company or project names, and
  other potentially identifying content from proposal content.

In 2017 and 2018, we accepted talk proposals via PaperCall and used other
off the shelf and customized tools for review and voting. These were all
connected by one-off migration scripts between each stage. In order to
effectively review 250+ proposals each year, we need everything under one
roof. We also used this as an opportunity to address some features high on
our wish list, including multiple presenters on a talk and a proposal form
more tailored to our needs.

## Enter Yak-Bak

Yak-Bak is an opinionated tool designed to support PyGotham's CFP and talk
selection process fully out of the box. PyGotham 2019 marks its first
production use. In the future, we'd like to offer this as a managed service
for conference organizers. If you'd like to support that effort, you can:

- Send us feedback at [yak-bak@bigapplepy.org](mailto:yak-bak@bigapplepy.org).
  If you're a user proposing a talk, let us know what you liked and didn't
  like about the process.
- [Contribute](https://gitlab.com/bigapplepy/yak-bak/) to the project. Open an
  issue, fix a bug, weigh in on discussions, etc. Everything helps. Yak-Bak, like
  PyGotham, is built for the community by the community.
- Yak-Bak should adapt to the branding of conferences that use it. If you
  have experience designing and building multi-tenant systems, we’d like
  your help and advice making the content management components right.
- If you're a conference organizer and would like to see more and consider
  using it for your conference, get in touch to discuss details.

If you have questions that aren't answered in this post, or if you'd just
like to talk more about Yak-Bak in general, feel free to reach out.
