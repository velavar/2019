---
name: Dropbox
tier: silver
site_url: https://dropbox.com/jobs/nyc
logo: dropbox.png
twitter: Dropbox
---
Dropbox is a modern workspace designed to reduce busywork so you can focus on the things that
matter. In our New York office, teams are driving significant impact towards Dropbox’s success,
owning a breadth of user-facing products and low-level infrastructure. Dropbox is a welcoming
environment for everyone, and we do our best to make sure all people feel supported and connected at
work. Come join us!
