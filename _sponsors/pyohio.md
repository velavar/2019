---
name: PyOhio
tier: community
site_url: https://www.pyohio.org/
logo: pyohio.png
twitter: PyOhio
---
PyOhio is the FREE annual Python community conference in Columbus, OH. Held the last weekend of
July, PyOhio includes four tracks of talks, tutorials, evening sprints and attendee receptions.
