require 'liquid'

module TitleCase
  def titlecase(text)
    return text.split(' ').map(&:capitalize).join(' ')
  end
end

Liquid::Template.register_filter(TitleCase)
