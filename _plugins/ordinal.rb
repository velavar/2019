# Jekyll-Ordinal: https://github.com/patrickcate/Jekyll-Ordinal
# Public domain under the Unlicense:
# https://github.com/patrickcate/Jekyll-Ordinal/blob/1bae09db582773c6e00bcca3d3cc95e68308d193/LICENSE
#
# Public: Print the ordinal of a Jekyll date.
#
# date - The Jekyll date String being passed.
# day  - The date variable formatted to the day of the month, without padded 0's.
#
# Examples
#
#   {{ page.date | ordinal }}
#   # => "st"
#
# Returns the ordinal String.

module Jekyll
  module Ordinal
    def ordinal(date)
      day = Date.parse(date).strftime("%-d");
      case day
      when "1", "21", "31";
        "st"
      when "2", "22";
        "nd"
      when "3", "23";
        "rd"
      else
        "th"
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Ordinal)
