---
name: Marianne Hoogeveen
talks:
- "The Cat\u0027s Alive! Add Some Weirdness To Your Code With Quantum Computing"
- "What physics can teach us about learning"
---
Marianne is a Staff Data Scientist for operations at Bowery Farming, where
she develops algorithms for robotics and supply chain to help Bowery grow
delicious crop efficiently.

She holds a MSc in Theoretical Physics, and a Ph.D. in Applied Mathematics,
during which she published on the dynamics of quantum entanglement in
nonequilibrium systems after a quench.

She left the academic world to do a fellowship at Insight Data Scinece, and
has worked at Cytora, Arena Analytics and General Assembly, before moving to
Bowery Farming.
