---
name: Jonathan Arfa
talks:
- "You Don\u0027t Need Neural Nets - How Simpler Algorithms Can Solve Your Problems With Less Headache"
---
Jonathan Arfa is a Texan expatriate living in New York City. He has worked
on Statistical and Machine Learning problems in the energy, ad-tech, and
dating industries.
