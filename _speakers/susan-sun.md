---
name: Susan Sun
talks:
- "How to size up a Python project for data freelancing"
---
Susan is a freelance data person. Her expertise lies in design-based
strategic data consulting for start-ups, data-related training & education,
conscientious application of data science for business, and pro bono data
work for social change.

Susan works in a variety of fields, from academia to industry, corporate to
nonprofit, including notable data leaders like New York Times, Google,
Columbia University, CUNY, General Assembly, and DataKind.
