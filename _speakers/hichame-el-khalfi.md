---
name: Hichame El Khalfi
talks:
- "How to Effectively Reduce AI Pipeline Runtime"
---
Hichame El Khalfi is the Data Engineering Lead for Hux at Deloitte Digital.
In this role, he leads the Data Engineering Team that supports building,
running and scaling the organization’s AI Decisioning capability within Hux,
the Deloitte Digital Experience services offering. Hichame focuses on
building and scaling reliable, automated and self-healing pipelines.

Prior to joining Deloitte Digital in 2018, Hichame worked at Magnetic and
several financial firms including SGCIB, Natixis and LBP.

Hichame holds a Master of Science in Distributed Systems Engineering and a
Master in Business Informatic from Université Paris-Est Créteil.
